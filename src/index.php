<?php
/**
 * Create a thumbnail
 *
 * @author Brett @ Mr PHP
 */
header('Access-Control-Allow-Origin: *');
require '../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::create(dirname(__DIR__));
$dotenv->load();

ini_set('memory_limit', '512M');
// ensure there was a thumb in the URL
if (!$_GET['thumb']) {
    error('no thumb');
}

// get the thumbnail from the URL
$thumb = strip_tags(htmlspecialchars($_GET['thumb']));

$isPdf = false;
if (strpos ($thumb, '.pdf.jpg') + 8 == strlen ($thumb)) {
    // this is a pdf
    $thumb = substr ($thumb, 0, -4);
    $isPdf = true;
}

// get info of the image from the url
$thumb_array = explode('/', $thumb);

//$algo = array_shift($thumb_array);

// rip the size from the thumb array
$size = array_shift($thumb_array);

// create the string to the original photo
$image_url = getenv ('STORAGE_URL') . '/' . implode('/', $thumb_array);

// set tmp dir
$tmp_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'tmp';
if (!is_dir ($tmp_dir)) {
    mkdir ($tmp_dir);
}

// set the path to the image
$image = $tmp_dir . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $thumb_array);

// if image hasn't been downloaded yet, download it
if (!file_exists ($image)) {
    $image_name = array_pop ($thumb_array);
    $target_dir = $tmp_dir . DIRECTORY_SEPARATOR . implode (DIRECTORY_SEPARATOR, $thumb_array);
    if (!is_dir ($target_dir)) {
        mkdir ($target_dir, 0777, true);
    }
    file_put_contents ($image, file_get_contents ($image_url));
}
list($width,$height) = explode('x', $size);


// ensure the image file exists
/*if (!file_exists($image)) {
    echo file_get_contents("/$size/no_image.png");
}*/
// generate the thumbnail
require 'phpthumb/phpthumb.class.php';
$phpThumb = new phpThumb();


$phpThumb->setSourceFilename($image);
$phpThumb->setParameter('config_document_root', $_SERVER['DOCUMENT_ROOT']);
$phpThumb->setParameter('disable_debug', false);
$phpThumb->setParameter('w', $width);
$phpThumb->setParameter('h', $height);
$phpThumb->setParameter('q', '100%');
$phpThumb->setParameter('ar', 'x');
$phpThumb->setParameter('config_imagemagick_path', "/usr/bin/convert");
$phpThumb->setParameter('f', substr($thumb, -3, 3)); // set the output format
$phpThumb->setParameter('far', 'C'); // scale outside
$phpThumb->setParameter('bg', 'FFFFFF'); // scale outside
if (!$phpThumb->GenerateThumbnail()) {
    
    error('cannot generate thumbnail');
}

$save_path = '/code/src/' . trim($thumb, '/') ;
if ($isPdf) {
    $save_path .= '.jpg';
}
// make the directory to put the image
if (!mkpath(dirname($save_path), true)) {
    error('cannot create directory');
}

// write the file
if (!$phpThumb->RenderToFile($save_path)) {
    
    error('cannot save thumbnail');
}

// redirect to the thumb
// note: you need the '?new' or IE wont do a redirect
header('Location: ' . $_SERVER['REQUEST_URI'] . '?new');

// basic error handling
function error($error)
{
    header("HTTP/1.0 404 Not Found");
    echo '<h1>Not Found</h1>';
    echo '<p>The image you requested could not be found.</p>';
    echo "<p>An error was triggered: <b>$error</b></p>";
    exit();
}
//recursive dir function
function mkpath($path, $mode)
{
    is_dir(dirname($path)) || mkpath(dirname($path), $mode);
    return is_dir($path) || @mkdir($path, 0777, $mode);
}
?>
